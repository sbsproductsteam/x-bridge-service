package com.serumula.xbridgeservice.service;

import com.serumula.xbridgeservice.api.dto.*;
import com.serumula.xbridgeservice.exception.InvalidInputException;
import org.apache.commons.vfs2.FileUtil;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@SpringBootTest
@ActiveProfiles("test")
public class StringToFileWriterServiceTest {

    public static String FILE_OUT_PUT_DIR = "";//"test/data/out/";
    @Autowired
    StringToFileWriterService stringToFileWriterService;

    @Autowired
    StringManipulationService stringManipulationService;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @AfterAll
    public  void cleanFiles(){
       //Delete the files so that the work space is clean of the testing files
    }

    @Test
    public void givenWritingToFile_whenUsingFileChannel_thenCorrect()
            throws IOException, InvalidInputException {
        BreauFileDto breauFileDto = stringManipulationService.getFileObject(getSampleTest());
        File file = new File("test.txt");
        if (new File(FILE_OUT_PUT_DIR+"test.txt").exists()) {
            file.delete();
        }
        streamFile(breauFileDto, file);
        Assertions.assertTrue(new File("test.txt").exists());


    }

    @Test
    public void givenDto_whenFromDto_thenReturnCorrectFile() throws IOException, InvalidInputException {
        BreauFileDto givenBeauFileDto  = stringManipulationService.getFileObject(getSampleTest());  //get the file from utils
        String givenFileName = FILE_OUT_PUT_DIR+"LSO128_CompuS_T702_M_20200921_01_01.txt";

        File actualFile =  stringToFileWriterService.fromDto(givenBeauFileDto, givenFileName);

        Assertions.assertNotNull(actualFile);


    }

    static void streamFile(BreauFileDto breauFileDto, File file) throws IOException {
        FileWriter fw = new FileWriter(file, true);
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(breauFileDto.getHeadRow());
        if (Objects.nonNull(breauFileDto.getDataRows()) && breauFileDto.getDataRows().size() > 0){
            for (BreauFileDto.Data data : breauFileDto.getDataRows()) {
                bw.newLine();
                bw.write(data.getDataRow());
            }
        }
        bw.newLine();
        bw.write(breauFileDto.getTailRow());
        bw.close();
    }

    private SegmentDto getSampleTest() {
        HeadSegment h = HeadSegment
                .builder()
                .header("H")
                .dateFileWasCreated("20200530")
                .supplierReferenceNumber("LS0630")
                .monthEndDate("20200701")
                .tradingNameOrBrandName("Serumula Financial Services")
                .versionNumber("06")
                .build();

        DataSegment dataSegment = DataSegment.builder()
                .data("D")
                .idNumber("012345678901")
//                .otherIdNumberOrPassportNumber()
                .gender("M")
                .dateAccountOpened("19870909")
                .accountNo("1234567890123456789012345")
                .subAccountNo("1234")
                .surname("Surname")
                .tittle("Prof")
                .foreNameOrInitial1("Initial One")
                .foreNameOrInitial2("Initial two")
                .foreNameOrInitial3("Initial three")
                .residentialAddressLine1("Mphosong hamonate")
                .typeOfAccount("SA")
                .dateAccountOpened("20140909")
                .currentBalance("4000")
                .currentBalanceIndicator("C")
                .instalmentAmount("1999")
                .statusDate("20150505").build();
        DataSegment dataSegment2 = DataSegment.builder()
                .data("D")
                .idNumber("840407678901")
//                .otherIdNumberOrPassportNumber()
                .gender("M")
                .dateAccountOpened("19870909")
                .accountNo("1234567890123456789012345")
                .subAccountNo("1234")
                .surname("Surname")
                .tittle("Prof")
                .foreNameOrInitial1("Initial One")
                .foreNameOrInitial2("Initial two")
                .foreNameOrInitial3("Initial three")
                .residentialAddressLine1("Mphosong hamonate")
                .typeOfAccount("SA")
                .dateAccountOpened("20140909")
                .currentBalance("4000")
                .currentBalanceIndicator("C")
                .instalmentAmount("1999")
                .statusDate("20150505").build();
        DataSegment dataSegment3 = DataSegment.builder()
                .data("D")
                .idNumber("803487498784")
                .otherIdNumberOrPassportNumber("RB403500")
                .gender("F")
                .dateAccountOpened("20200909")
                .accountNo("7774567890123456789012345")
                .subAccountNo("545")
                .surname("Molewa")
                .tittle("MR")
                .foreNameOrInitial1("P")
                .foreNameOrInitial2("D")
                .foreNameOrInitial3("NOn")
                .residentialAddressLine1("Centurion hamonate")
                .typeOfAccount("SA")
                .currentBalance("900")
                .currentBalanceIndicator("C")
                .instalmentAmount("209")
                .statusDate("20150505").build();

        TailSegment t = TailSegment.builder()
                .trailer("T")
                .numberOfRecordsSupplied("1")
                .filler("")
                .build();

        List<DataSegment> dataSegments = new ArrayList<>();
        dataSegments.add(dataSegment);
        dataSegments.add(dataSegment2);
        dataSegments.add(dataSegment3);

        return SegmentDto.builder().dataSegments(dataSegments).headSegment(h).tailSegment(t).build();
    }
}
