package com.serumula.xbridgeservice.service ;

import com.serumula.xbridgeservice.api.dto.*;
import com.serumula.xbridgeservice.api.util.DataUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class StringManipulationServiceTest {

    @Mock
    DataSegment dataSegment;

    @Mock
    StringManipulationService stringManipulationService;


    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    @Disabled("FIXME this test should run its critical on the way string works, looks like empty string works different in java 11 ")
    public void testStringShouldReturnLeftPaddedStringWithSpaces() throws Exception {
        String fieldInputValue1 = "GeeksForGeeks";
        int fieldInputValueLengthExpected = 20;

        assertEquals(13, fieldInputValue1.length());
        String paddedString = DataUtil.leftPadding("",fieldInputValue1, fieldInputValueLengthExpected);
        assertEquals(20,paddedString.length());

        String fieldInputValue2 = "12345678GeeksForGeeks";
        assertEquals(21, fieldInputValue2.length());
        String paddedString2 = DataUtil.leftPadding("",fieldInputValue2, fieldInputValueLengthExpected);
       assertEquals(20, paddedString2.length());

    }

    @Test
    public void testNullStringShouldReturnStringWithSpaces() throws Exception {
        String fieldInputValue1 = "";
        int fieldInputValueLengthExpected = 20;

        assertEquals(0, fieldInputValue1.length());
        String paddedString = DataUtil.rightPadding("",fieldInputValue1, fieldInputValueLengthExpected);
        assertEquals(20,paddedString.length());

        String fieldInputValue2 = null ;
        assertNull( fieldInputValue2);
        String paddedString2 = DataUtil.rightPadding("", fieldInputValue2, fieldInputValueLengthExpected);
        assertEquals(20, paddedString2.length());

    }

    @Test
    @Disabled("FIXME this test should run its critical on the way string works, looks like empty string works different in java 11 ")
    public  void  givenDataFromUIWhenCalled_then_return_valid_string(){
        try {
            BreauFileString breauFile = stringManipulationService.getFile(getSampleTest());
           String expectedCompuScanString =  breauFile != null?
                   breauFile.getAllRows(): null;

            assertNotNull(expectedCompuScanString);


        }
        catch (Exception e){
         e.printStackTrace(System.err);
        }
    }

    private SegmentDto getSampleTest(){
        HeadSegment h = HeadSegment
                .builder()
                .header("H")
                .dateFileWasCreated("20200530")
                .supplierReferenceNumber("LS0630")
                .monthEndDate("20200701")
                .tradingNameOrBrandName("Serumula Financial Services")
                .versionNumber("06")
                .build();

        DataSegment dataSegment = DataSegment.builder()
                .data("D")
                .idNumber("0123456789012")
//                .otherIdNumberOrPassportNumber()
                .gender("M")
                .dateAccountOpened("19870909")
                .accountNo("1234567890123456789012345")
                .subAccountNo("1234")
                .surname("Surname")
                .tittle("Prof")
                .foreNameOrInitial1("Initial One")
                .foreNameOrInitial2("Initial two")
                .foreNameOrInitial3("Initial three")
                .residentialAddressLine1("Mphosong hamonate")
                .typeOfAccount("SA")
                .dateAccountOpened("20140909")
                .currentBalance("4000")
                .currentBalanceIndicator("C")
                .instalmentAmount("1999")
                .statusDate("20150505").build();

        TailSegment t = TailSegment.builder()
                .trailer("T")
                .numberOfRecordsSupplied("1")
                .filler("")
                .build();

        List<DataSegment> dataSegments = new ArrayList<>();
        dataSegments.add(dataSegment);

        return SegmentDto.builder().dataSegments(dataSegments).headSegment(h).tailSegment(t).build();
    }
}
