package com.serumula.xbridgeservice.api.util;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class CompuScanTest {


    @Mock
    List<String> mockedList;

    @Spy
    List<String> spiedList = new ArrayList<String>();

    StringBuilder  stringBuilder = new StringBuilder();

    static String myString  = writeAtFixedLength("", 700);

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testNullStringShouldReturnStringWithSpaces() throws Exception {
        String fixedString = writeAtFixedLength(null, 5);
     assertEquals(fixedString, "     ");
    }

    @Test
    public void testEmptyStringReturnStringWithSpaces() throws Exception {
        String fixedString = writeAtFixedLength("", 5);
        assertEquals(fixedString, "     ");
    }

    @Test
    @Disabled("FIXME this test should run its critical on the way string works, looks like empty string works different in java 11 ")
    public void testShortString_ReturnSameStringPlusSpaces() throws Exception {
        String fixedString = writeAtFixedLength("aa", 5);
        assertEquals(fixedString, "aa   ");
        assertEquals(fixedString.length(), 5);

        String fixedStringEmptyLong = writeAtFixedLength("", 700);
        assertEquals(fixedStringEmptyLong.length(), 5);

//        fixedStringEmptyLong.re


        StringBuilder myString = new StringBuilder(fixedStringEmptyLong);
//        myString.setCharAt(fixedStringEmptyLong, "a");


//        final String replaceString = fixedStringEmptyLong.substring(10,17);


    }

    @Test
    public void testLongStringShouldBeCut() throws Exception {
        String fixedString = leftpad("aaaaaaaaaa", 5);
        assertEquals(fixedString, "aaaaa");
    }


    private static String writeAtFixedLength(String pString, int lenght) {
        if (pString != null && !pString.isEmpty()){
            return getStringAtFixedLength(pString, lenght);
        }else{
            return completeWithWhiteSpaces("", lenght);
        }
    }

    private static String getStringAtFixedLength(String pString, int lenght) {
        if(lenght < pString.length()){
            return pString.substring(0, lenght);
        }else{
            return completeWithWhiteSpaces(pString, lenght - pString.length());
        }
    }

    private static String completeWithWhiteSpaces(String pString, int lenght) {
        for (int i=0; i<lenght; i++)
            pString += " ";
        return pString;
    }

    private static String generateIdNumber(String stuffStudent, String schoolCode, int currentCount) {
        String formatter = String.format("%05d", currentCount + 1); // eg 00012
        return stuffStudent + schoolCode + formatter;
    }


    private static String formatFieldPadLeft(String fieldValue, int fieldValueExpectedLength) {
        String formatter = String.format("%[a-zA-Z0-9]{5}", fieldValue);
        return formatter;
    }

    private String leftpad(String text, int length) {
        return String.format("%" + length + "." + length + "s", text);
    }

    private String rightpad(String text, int length) {
        return String.format("%-" + length + "." + length + "s", text);
    }

    // Get the column
    // pad the data, left or right
    // add the field to the long String

    String rowIndicator = "D"; //1. -> 1-1
    String idNumber ="059163183222"; //2.-> 2-14
    String passportNumber ; //3. -> 15-30
    String gender;   //4. -> 31-31
    String dateOfBirth ; //5. -> 32-39
    String branchCode; //6. -> 40-47


}
