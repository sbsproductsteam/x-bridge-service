package com.serumula.xbridgeservice.ftransfer;

import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemManager;
import org.apache.commons.vfs2.Selectors;
import org.apache.commons.vfs2.VFS;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

@SpringBootTest
@ActiveProfiles("test")
class SFTPProviderTest {

    @Value("${compuscan-sftp.host-name}" )
    private  String hostName;
    @Value("${compuscan-sftp.username}" )
    private String  username;
    @Value("${compuscan-sftp.password}" )
    private String  password;
    @Value("${compuscan-sftp.remote-file-path}" )
    private String  remoteFilePath;

    private String localFilePath = "";


    @Test
    public void whenUploadFile_thenSuccess() throws IOException {

        File f = ResourceUtils.getFile("LSO128_CompuS_T702_M_20200504_01_01.txt");
        if (!f.exists()) {
            throw new RuntimeException("Error. Local file not found");
        }


        FileSystemManager manager = VFS.getManager();

        FileObject localFile = manager.resolveFile(f.getAbsolutePath());

        FileObject remote = manager.resolveFile(createConnectionString());

        remote.copyFrom(localFile, Selectors.SELECT_SELF);

        localFile.close();
        remote.close();
    }

    private String createConnectionString() {
        String userInfo = username + ":" + password;
        URI uri = null;
        try {
            uri = new URI("sftp", userInfo, hostName, -1,
                    remoteFilePath + "LSO128_CompuS_T702_M_20200504_01_01.txt", null, null);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return uri.toString();
    }

}
