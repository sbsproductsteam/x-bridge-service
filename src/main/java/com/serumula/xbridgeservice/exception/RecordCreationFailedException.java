package com.serumula.xbridgeservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.EXPECTATION_FAILED)
public class RecordCreationFailedException extends Exception {
    private static final long serialVersionUID = 1L;

    public RecordCreationFailedException() {
        super();
    }

    public RecordCreationFailedException(String message) {
        super(message);
    }

    public RecordCreationFailedException(String message, Throwable t) {
        super(message, t);
    }
}
