package com.serumula.xbridgeservice.service;

import com.serumula.xbridgeservice.api.dto.BreauFileDto;
import com.serumula.xbridgeservice.api.dto.BreauFileString;
import com.serumula.xbridgeservice.api.dto.CompuscanRequest;
import com.serumula.xbridgeservice.exception.InvalidInputException;
import com.serumula.xbridgeservice.ftransfer.SFTPProvider;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Objects;

import static java.util.Objects.*;

@Service
@AllArgsConstructor
public class StringToFileWriterService {

    private final StringManipulationService stringManipulationService;
    private final SFTPProvider sftpProvider;

    public void readFromTXTFile(){

    }

    public void writeToTXTFile(){

    }

    public File fromDto(BreauFileDto breauFileDto, String fileName) throws IOException {
            File file = new File(fileName);
            if (file.exists()) {
                file.delete();
            }
        streamFile(breauFileDto, file);
        return file;
    }


    // FIXME Please let this method to return some kind of boolean or  enum [SUBMITTED, SUCCESSFULLY_SUBMITTED, FAILED_SUBMISSION]
    // FIXME exception caught could mean there FAILED_SUBMISSION
    // FIXME Maybe Response object that has status and message will do
    //FIXME Test the method
    public void sendFileToCompuscan(CompuscanRequest compuscanRequest) throws InvalidInputException, IOException {
        File file = fromDto(stringManipulationService.getFileObject(compuscanRequest.getSegmentDto()), compuscanRequest.getCompuscanHeaderDto().getFileName());
        sftpProvider.upload(compuscanRequest.getCompuscanHeaderDto(), file);

    }

    public static void streamFile(BreauFileDto breauFileDto, File file) throws IOException {
        FileWriter fw = new FileWriter(file, true);
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(breauFileDto.getHeadRow());
        if (nonNull(breauFileDto.getDataRows()) && breauFileDto.getDataRows().size() > 0){
            for (BreauFileDto.Data data : breauFileDto.getDataRows()) {
                bw.newLine();
                bw.write(data.getDataRow());
            }
        }
        bw.newLine();
        bw.write(breauFileDto.getTailRow());
        bw.close();
    }
}
