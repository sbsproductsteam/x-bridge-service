package com.serumula.xbridgeservice.service;


import com.serumula.xbridgeservice.api.dto.BreauFileDto;
import com.serumula.xbridgeservice.api.dto.BreauFileString;
import com.serumula.xbridgeservice.api.dto.DataSegment;
import com.serumula.xbridgeservice.api.dto.SegmentDto;
import com.serumula.xbridgeservice.exception.InvalidInputException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Slf4j
@Service
public class StringManipulationService {

    public BreauFileString getFile(SegmentDto segmentDto)
            throws InvalidInputException {
        log.debug(" > {}", segmentDto.toString());
        return  BreauFileString.builder().allRows(segmentDto.toCompuscanString()).build();
    }

    public BreauFileDto getFileObject(SegmentDto segmentDto) throws InvalidInputException {
        ArrayList<BreauFileDto.Data> dataArrayList = new ArrayList<>();
        for (final DataSegment dataSegment : segmentDto.getDataSegments()) {
            BreauFileDto.Data data = new BreauFileDto.Data();
            data.setDataRow(dataSegment.toCompuScanString());
            dataArrayList.add(data);
        }
        segmentDto.getTailSegment().setNumberOfRecordsSupplied(String.valueOf(dataArrayList.size()));
        return BreauFileDto.builder()
                .dataRows(dataArrayList)
                .headRow(segmentDto.getHeadSegment().toCompuScanString())
                .tailRow(segmentDto.getTailSegment().toCompuScanString())
                .build();
    }
}
