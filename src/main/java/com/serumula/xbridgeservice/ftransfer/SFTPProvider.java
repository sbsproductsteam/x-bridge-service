package com.serumula.xbridgeservice.ftransfer;

import com.serumula.xbridgeservice.api.dto.CompuscanHeaderDto;
import com.serumula.xbridgeservice.config.properties.CompuscanProperties;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.vfs2.*;
import org.apache.commons.vfs2.impl.StandardFileSystemManager;
import org.apache.commons.vfs2.provider.sftp.SftpFileSystemConfigBuilder;
import org.springframework.stereotype.Component;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;

@Component
@Slf4j
@AllArgsConstructor
public class SFTPProvider {

    private final CompuscanProperties  compuscanProperties;

    public void upload(CompuscanHeaderDto compuscanHeaderDto, File f) {

        if (!f.exists()) {
            throw new RuntimeException("Error. payment file not found");
        }

        StandardFileSystemManager manager = new StandardFileSystemManager();

        try {
            manager.init();
            // Create local file object
            FileObject localFile = manager.resolveFile(f.getAbsolutePath());
            // Create remote file object
            FileObject remoteFile = manager.resolveFile(
                    createConnectionString(compuscanHeaderDto), createDefaultOptions());
            // Copy local file to sftp server
            remoteFile.copyFrom(localFile, Selectors.SELECT_SELF);
            log.info("File upload success");
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            manager.close();
        }
    }

    public void download(String fileName) {

        StandardFileSystemManager manager = new StandardFileSystemManager();

        try {
            manager.init();

            String downloadFilePath = "/";
            // Create local file object
            FileObject localFile = manager.resolveFile(downloadFilePath);
            // Create remote file object
            FileObject remoteFile = manager.resolveFile(
                    createConnectionString(fileName), createDefaultOptions());
            // Copy local file to sftp server
            localFile.copyFrom(remoteFile, Selectors.SELECT_SELF);
            log.info("File download success");
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            manager.close();
        }
    }

    public void delete(String fileName) {
        StandardFileSystemManager manager = new StandardFileSystemManager();
        try {
            manager.init();
            // Create remote object
            FileObject remoteFile = manager.resolveFile(
                    createConnectionString(fileName), createDefaultOptions());
            if (remoteFile.exists()) {
                remoteFile.delete();
                log.info("Delete remote file success");
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            manager.close();
        }
    }

    public boolean exist(String fileName) {
        StandardFileSystemManager manager = new StandardFileSystemManager();

        try {
            manager.init();
            // Create remote object
            FileObject remoteFile = manager.resolveFile(
                    createConnectionString(fileName), createDefaultOptions());
            boolean  fileExists =  remoteFile.exists();
            log.info("File exist: " + fileExists);

            return fileExists;
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            manager.close();
        }
    }

    private String createConnectionString(String fileName) {
        String userInfo = compuscanProperties.getUsername() + ":" + compuscanProperties.getPassword();
        URI uri = null;
        try {
            uri = new URI("sftp", userInfo, compuscanProperties.getHostName(), -1,
                    compuscanProperties.getRemoteFilePath() + fileName, null, null);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return uri.toString();
    }

    private String createConnectionString(CompuscanHeaderDto compuscanHeaderDto) {
        String userInfo = compuscanHeaderDto.getSftpUsername() + ":" + compuscanHeaderDto.getSftpPassword();
        URI uri = null;
        try {
            uri = new URI("sftp", userInfo, compuscanProperties.getHostName(), -1,
                    compuscanProperties.getRemoteFilePath() + compuscanHeaderDto.getFileName(), null, null);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return uri.toString();
    }

    private FileSystemOptions createDefaultOptions()
            throws FileSystemException {
        // Create SFTP options
        FileSystemOptions opts = new FileSystemOptions();
        // SSH Key checking
        SftpFileSystemConfigBuilder.getInstance().setStrictHostKeyChecking(
                opts, "no");
        // Root directory set to user home
        SftpFileSystemConfigBuilder.getInstance().setUserDirIsRoot(opts, true);
        // Timeout is count by Milliseconds
        SftpFileSystemConfigBuilder.getInstance().setTimeout(opts, 10000);
        return opts;
    }

    //    private String createConnectionString() {
//        return "sftp://" + compuscanProperties.getUsername() + ":" + compuscanProperties.getPassword() + "@" + compuscanProperties.getHostName() + "/"
//                + compuscanProperties.getRemoteFilePath();
//    }
}
