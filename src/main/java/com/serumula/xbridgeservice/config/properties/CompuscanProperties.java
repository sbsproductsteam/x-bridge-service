package com.serumula.xbridgeservice.config.properties;

import lombok.*;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "compuscan-sftp")
@Setter
@Getter
@ToString
public class CompuscanProperties {
    private  String hostName;
    private String  username;
    private String  password;
    private String  remoteFilePath;

}
