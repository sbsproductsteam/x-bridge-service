package com.serumula.xbridgeservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XBridgeServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(XBridgeServiceApplication.class, args);
	}

}
