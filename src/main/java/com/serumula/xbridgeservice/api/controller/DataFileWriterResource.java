package com.serumula.xbridgeservice.api.controller;

import com.serumula.xbridgeservice.api.dto.BreauFileDto;
import com.serumula.xbridgeservice.api.dto.BreauFileString;
import com.serumula.xbridgeservice.api.dto.CompuscanRequest;
import com.serumula.xbridgeservice.api.dto.SegmentDto;
import com.serumula.xbridgeservice.exception.InvalidInputException;
import com.serumula.xbridgeservice.service.StringManipulationService;
import com.serumula.xbridgeservice.service.StringToFileWriterService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;


@RestController
@RequestMapping(path = "/compuscan-api")
@Api(value = "Provides formatted file as  per CompuScan", description = "Only provides and send file to CompuScan", tags = "Loan Management System")
@Validated
@Slf4j
@AllArgsConstructor
public class DataFileWriterResource {

    private final StringManipulationService stringManipulationService;
    private final StringToFileWriterService stringToFileWriterService;

    @ApiOperation(value = "gets the formatted file as per CompuScan spec", response = BreauFileString.class
            , tags = "Breau Data")
    @PostMapping(path = "/send-to-compuscan")
    public String sendFileToCompuscan(@RequestBody CompuscanRequest compuscanRequest) throws InvalidInputException, IOException {
        log.debug(" > {}", compuscanRequest);
        stringToFileWriterService.sendFileToCompuscan(compuscanRequest);
        return "";
    }

    @ApiOperation(value = "gets the formatted file as per CompuScan spec", response = BreauFileString.class
            , tags = "Breau Data")
    @PostMapping(path = "/download-file-string")
    public BreauFileString getFile(@RequestBody SegmentDto segmentDto)
            throws InvalidInputException {
        log.debug(" > {}", segmentDto.toCompuscanString());
        return stringManipulationService.getFile(segmentDto);
    }

    @ApiOperation(value = "gets the formatted file as per CompuScan spec", response = BreauFileDto.class
            , tags = "Breau Data")
    @PostMapping(path = "/download-file-object")
    public BreauFileDto getFileObject(@RequestBody SegmentDto segmentDto)
            throws InvalidInputException {
        log.debug(" > {}", segmentDto.toCompuscanString());
        return stringManipulationService.getFileObject(segmentDto);
    }

}
