package com.serumula.xbridgeservice.api.util;


import com.serumula.xbridgeservice.exception.InvalidInputException;
import org.springframework.util.StringUtils;

public class DataUtil {
    // Function to perform left padding
    // left pad the string
    // with space up to length fieldInputValueLengthExpected
    public static String
    leftPadding(String fieldName,String fieldInputValue, int fieldInputValueLengthExpected) throws InvalidInputException {
        return padString(fieldName,fieldInputValue, fieldInputValueLengthExpected);
    }

    // Function to perform right padding
    // right pad the string
    // with space up to length fieldInputValueLengthExpected
    public static String
    rightPadding(String fieldName, String fieldInputValue, int fieldInputValueLengthExpected) throws InvalidInputException {
        return padString(fieldName, fieldInputValue, -(fieldInputValueLengthExpected));
    }

    private static String
    padString(String fieldName, String fieldInputValue, int fieldInputValueLengthExpected) throws InvalidInputException {
        if(!StringUtils.isEmpty(fieldInputValue) && fieldInputValue.length()> Math.abs(fieldInputValueLengthExpected))
            throw new InvalidInputException("Value for { Field ->  "+fieldName+ "  } value must not have length greater than  ->" + Math.abs(fieldInputValueLengthExpected));
        if(StringUtils.isEmpty(fieldInputValue))
            fieldInputValue="";
        return String
                .format("%" + (fieldInputValueLengthExpected) + "s", fieldInputValue);
    }
}
