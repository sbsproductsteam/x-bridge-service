package com.serumula.xbridgeservice.api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.serumula.xbridgeservice.api.util.DataUtil;
import com.serumula.xbridgeservice.exception.InvalidInputException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Data
@SuperBuilder
@Builder
public class HeadSegment {
    /*1*/private String header = "H"; //1-1 -> A1 ->M
    /*2*/@JsonProperty(required = true) @NotBlank String supplierReferenceNumber = ""; //2-11 -> A10 ->M
    /*3*/private String monthEndDate = ""; //12-19 -> N8 ->M
    /*4*/private String versionNumber = ""; //20-21 -> N2 ->M
    /*5*/private String dateFileWasCreated = ""; //22-29 -> N8 ->M
    /*6*/private String tradingNameOrBrandName = ""; //30-89 -> A60 ->M
    /*7*/private String filler = ""; //90-700

    public String toCompuScanString() throws InvalidInputException {
        StringBuilder s = new StringBuilder();
        s
                .append(DataUtil.rightPadding("Non padded -> header @position 1-1", this.header,1))
                .append(DataUtil.leftPadding("Left padded -> supplierReferenceNumber @position 2-11",this.supplierReferenceNumber, 10))
                .append(DataUtil.rightPadding("Right padded -> monthEndDate  @position 12-19",this.monthEndDate, 8))
                .append(DataUtil.rightPadding("Right padded -> versionNumber @position 20-21", this.versionNumber, 2))
                .append(DataUtil.leftPadding("Left padded -> dateFileWasCreated @position 22-29 ",this.dateFileWasCreated, 8))
                .append(DataUtil.rightPadding("Right padded -> tradingNameOrBrandName @position 30-89", this.tradingNameOrBrandName, 60))
                .append(DataUtil.rightPadding("Right padded -> filler @position 90-700",this.filler, 611));
        return s.toString();
    }
}
