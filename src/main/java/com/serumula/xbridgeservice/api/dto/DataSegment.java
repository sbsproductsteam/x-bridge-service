package com.serumula.xbridgeservice.api.dto;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.serumula.xbridgeservice.api.util.DataUtil;
import com.serumula.xbridgeservice.exception.InvalidInputException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.util.StringUtils;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Data
@SuperBuilder
@Builder
public class DataSegment {
    // Get the column
    // pad the data, left or right
    // add the field to the long String

    /*1*/@JsonProperty(required = true) @NotBlank  String data = "D"; //1-1 -> A1 ->M
    /*2*/String idNumber =""; //2-14 -> N13 ->M ->R
    /*3*/String otherIdNumberOrPassportNumber =""; //15-30 ->A16 ->0
    /*4*/String gender =""; //31-31 -> A1 ->M
    /*5*/String dateOfBirth =""; //32-39 ->N8 -> C
    /*6*/String branchCode =""; //40-47 ->A8 ->R
    /*7*/@JsonProperty(required = true) @NotBlank  String accountNo ="";//48-72 -> A25 ->M
    /*8*/@JsonProperty(required = true) @NotBlank  String subAccountNo=""; //73-76 -> A4 ->M
    /*9*/@JsonProperty(required = true) @NotBlank  String surname=""; //77-101 -> A25 ->M
    /*10*/String tittle ="";//102-106 -> A5
    /*11*/@JsonProperty(required = true) @NotBlank  String foreNameOrInitial1="";//107-120 -> A14 ->M
    /*12*/String foreNameOrInitial2="";//121-134 -> A14
    /*13*/String foreNameOrInitial3="";//135-148 -> A14
    /*14*/String residentialAddressLine1="";//149-173 ->A25 ->C
    /*15*/String residentialAddressLine2="";//174-198 ->A25 ->C
    /*16*/String residentialAddressLine3="";//199-223 ->A25
    /*17*/String residentialAddressLine4="";//224-248 ->A25
    /*18*/String postalCodeOfResidentialAddress="";//249-254 ->A6 ->C ->R
    /*19*/String ownerOrTenant="";//255-255 -> A1
    /*20*/String postalAddressLine1="";//256-280 ->A25 -> C
    /*21*/String postalAddressLine2="";//281-305 ->A25 -> C
    /*22*/String postalAddressLine3="";//306-330 ->A25
    /*23*/String postalAddressLine4="";//331-355 ->A25
    /*24*/String postalCodeOfPostalAddress="";//356-361 ->A6 -> C ->R
    /*25*/String ownershipType="";//362-363->A2 -> C
    /*26 (formerEndUseCodeNlrAccounts)*/String loanReasonCode="";//364-365 ->A2 -> C
    /*27*/String paymentType="";//366-367 ->A2 -> C
    /*28*/@JsonProperty(required = true) @NotBlank  String typeOfAccount="";//368-369 ->A2 ->M
    /*29*/@JsonProperty(required = true) @NotBlank  String dateAccountOpened="";//370-377 ->N8 ->M
    /*30*/String deferredPaymentDate="";//378-385 ->N8 ->C
    /*31*/String dateOnWhichLastPaymentWasReceived ="";//386-393 ->N8 ->C
    /*32*/String openingBalanceOrCreditLimit="";//394-402 ->N9 ->C
    /*33*/@JsonProperty(required = true) @NotBlank  String currentBalance="";//403-411 ->N9 ->M
    /*34*/@JsonProperty(required = true) @NotBlank  String currentBalanceIndicator="";//412-412 ->A1 ->M
    /*35*/String amountOverdue="";//413-421 ->N9 ->C
    /*36*/@JsonProperty(required = true) String instalmentAmount="";//422-430 ->N9 ->M
    /*37*/String monthsInArrears="";//431-432 ->N2 ->C
    /*38*/String statusCode="";//433-434 ->A2 ->C
    /*39*/String repaymentFrequency="";//435-436 ->N2 ->C
    /*40*/String terms="";//437-440 ->N4 ->C
    /*41*/@JsonProperty(required = true) @NotBlank  String statusDate="";//441-448 ->N8 ->M if A
    /*42*/@JsonProperty(required = true) @NotBlank  String oldSupplierBranchCode="";//449-456 ->A8 ->M if A
    /*43*/@JsonProperty(required = true) @NotBlank  String oldAccountNumber="";//457-481 ->A25 ->M if A
    /*44*/@JsonProperty(required = true) @NotBlank  String oldSubAccountNumber="";//482-485 ->A4 ->M if A
    /*45*/@JsonProperty(required = true) @NotBlank  String oldSupplierReferenceNumber="";//486-495 ->A10 ->M if A
    /*46*/String homeTelephone="";//496-4511 ->A16 ->C ->R
    /*47*/String cellularTelephone="";//512-527 ->A16 ->C->R
    /*48*/String workTelephone="";//528-543 ->A16 ->C -> R
    /*49*/String employerDetail="";//544-603 ->A60 ->O
    /*50*/String income="";//604-612 ->N9 ->O
    /*51*/String incomeFrequency="";//613-613 ->A1 ->C
    /*52*/String occupation="";//614-633 ->A20 ->O
    /*53*/String thirdPartyName="";//634-693 ->A60 ->O
    /*54*/String accountSoldToThirdParty="";//694-695 ->N2 ->C
    /*55*/String noOfParticipantsInJointLoan="";//696-698 ->N3 ->C
    /*56*/String filler="";//699-700 ->A2

    private   void validateInput() throws InvalidInputException {
        if (StringUtils.isEmpty(this.data))
            throw new InvalidInputException(" Segment indicator cannot be null");
        if (StringUtils.isEmpty(this.idNumber) && (StringUtils.isEmpty(otherIdNumberOrPassportNumber) || StringUtils.isEmpty(dateOfBirth)) )
            throw new InvalidInputException(" One of Id Number or (Passport and DOB) cannot be null");
        if (StringUtils.isEmpty(this.gender))
            throw new InvalidInputException(" gender cannot be null");
        if (StringUtils.isEmpty(this.accountNo))
            throw new InvalidInputException(" accountNo cannot be null");
        if (StringUtils.isEmpty(this.subAccountNo))
            throw new InvalidInputException(" subAccountNo cannot be null");
        if (StringUtils.isEmpty(this.surname))
            throw new InvalidInputException(" surname cannot be null");
        if (StringUtils.isEmpty(this.foreNameOrInitial1))
            throw new InvalidInputException(" foreNameOrInitial1 cannot be null");
        if (StringUtils.isEmpty(this.typeOfAccount))
            throw new InvalidInputException(" typeOfAccount cannot be null");
        if (StringUtils.isEmpty(this.dateAccountOpened))
            throw new InvalidInputException(" dateAccountOpened cannot be null");
        if (StringUtils.isEmpty(this.currentBalanceIndicator))
            throw new InvalidInputException(" currentBalanceIndicator cannot be null");
        if (StringUtils.isEmpty(this.instalmentAmount))
            throw new InvalidInputException(" instalmentAmount cannot be null");
        if (StringUtils.isEmpty(this.statusDate) && !StringUtils.isEmpty(statusCode))
            throw new InvalidInputException(" statusDate cannot be null");
//        if (StringUtils.isEmpty(this.oldSupplierBranchCode))
//            throw new InvalidInputException(" oldSupplierBranchCode cannot be null");
//        if (StringUtils.isEmpty(this.oldSubAccountNumber))
//            throw new InvalidInputException(" oldSubAccountNumber cannot be null");
//        if (StringUtils.isEmpty(this.oldSupplierReferenceNumber))
//            throw new InvalidInputException(" oldSupplierReferenceNumber cannot be null");
    }

    public String toCompuScanString() throws InvalidInputException {
        this.validateInput();
        StringBuilder s = new StringBuilder();
        s
                .append(DataUtil.rightPadding("Right padded -> data @Position 1-1",this.data,1))
                .append(DataUtil.leftPadding("Left padded -> idNumber @Position 2-14", this.idNumber, 13))
                .append(DataUtil.rightPadding("Right padded -> otherIdNumberOrPassportNumber @Position 15-30",this.otherIdNumberOrPassportNumber, 16))
                .append(DataUtil.rightPadding("Right padded -> gender @Position 31-31",this.gender, 1))
                .append(DataUtil.leftPadding("Left padded -> dateOfBirth @Position 32-39",this.dateOfBirth, 8))
                .append(DataUtil.leftPadding("Left padded -> branchCode @Position 40-47",this.branchCode, 8))
                .append(DataUtil.leftPadding("Left padded -> accountNo @Position 48-72",this.accountNo, 25))
                .append(DataUtil.leftPadding("Left padded -> subAccountNo @Position 73-76",this.subAccountNo, 4))
                .append(DataUtil.rightPadding("Right padded -> surname @Position 77-101",this.surname, 25))
                .append(DataUtil.rightPadding("Right padded -> tittle @Position 102-106",this.tittle, 5))
                .append(DataUtil.rightPadding("Right padded -> foreNameOrInitial1 @Position 107-120",this.foreNameOrInitial1, 14))
                .append(DataUtil.rightPadding("Right padded -> foreNameOrInitial2 @Position 121-134",this.foreNameOrInitial2, 14))
                .append(DataUtil.rightPadding("Right padded -> foreNameOrInitial3 @Position 135-148",this.foreNameOrInitial3, 14))
                .append(DataUtil.rightPadding("Right padded -> residentialAddressLine1 @Position 149-173",this.residentialAddressLine1, 25))
                .append(DataUtil.rightPadding("Right padded -> residentialAddressLine2 @Position 174-198",this.residentialAddressLine2, 25))
                .append(DataUtil.rightPadding("Right padded -> residentialAddressLine3 @Position 199-223",this.residentialAddressLine3, 25))
                .append(DataUtil.rightPadding("Right padded -> residentialAddressLine4 @Position 224-248",this.residentialAddressLine4, 25))
                .append(DataUtil.leftPadding("Left padded -> postalCodeOfResidentialAddress @Position 249-254",this.postalCodeOfResidentialAddress, 6))
                .append(DataUtil.rightPadding("Right padded -> ownerOrTenant @Position 255-255",this.ownerOrTenant, 1))
                .append(DataUtil.rightPadding("Right padded -> postalAddressLine1 @Position 256-280",this.postalAddressLine1, 25))
                .append(DataUtil.rightPadding("Right padded -> postalAddressLine2 @Position 281-305",this.postalAddressLine2, 25))
                .append(DataUtil.rightPadding("Right padded -> postalAddressLine3 @Position 306-330",this.postalAddressLine3, 25))
                .append(DataUtil.rightPadding("Right padded -> postalAddressLine4 @Position 331-355",this.postalAddressLine4, 25))
                .append(DataUtil.leftPadding("Left padded -> Left padded ->postalCodeOfPostalAddress @Position 356-361",this.postalCodeOfPostalAddress, 6))
                .append(DataUtil.rightPadding("Right padded -> ownershipType @Position 362-363",this.ownershipType, 2))
                .append(DataUtil.rightPadding("Right padded -> loanReasonCode @Position 364-365",this.loanReasonCode, 2))
                .append(DataUtil.rightPadding("Right padded -> paymentType @Position 366-367",this.paymentType, 2))
                .append(DataUtil.rightPadding("Right padded -> typeOfAccount @Position 368-369",this.typeOfAccount, 2))
                .append(DataUtil.leftPadding("Left padded -> dateAccountOpened @Position 370-377",this.dateAccountOpened, 8))
                .append(DataUtil.leftPadding("Left padded -> deferredPaymentDate @Position 378-385",this.deferredPaymentDate, 8))
                .append(DataUtil.leftPadding("Left padded -> dateOnWhichLastPaymentWasReceived @Position 386-393",this.dateOnWhichLastPaymentWasReceived, 8))
                .append(DataUtil.leftPadding("Left padded -> openingBalanceOrCreditLimit @Position 394-402",this.openingBalanceOrCreditLimit, 9))
                .append(DataUtil.leftPadding("Left padded -> currentBalance @Position 403-411",this.currentBalance, 9))
                .append(DataUtil.rightPadding("Right padded -> currentBalanceIndicator @Position 412-412",this.currentBalanceIndicator, 1))
                .append(DataUtil.leftPadding("Left padded -> amountOverdue @Position 413-421",this.amountOverdue, 9))
                .append(DataUtil.leftPadding("Left padded -> instalmentAmount @Position 422-430",this.instalmentAmount, 9))
                .append(DataUtil.leftPadding("Left padded -> monthsInArrears @Position 431-432",this.monthsInArrears, 2))
                .append(DataUtil.rightPadding("Right padded-> Right padded->statusCode @Position 433-434",this.statusCode, 2))
                .append(DataUtil.leftPadding("Left padded -> repaymentFrequency @Position 435-436",this.repaymentFrequency, 2))
                .append(DataUtil.leftPadding("Left padded -> terms @Position 437-440",this.terms, 4))
                .append(DataUtil.leftPadding("Left padded -> statusDate @Position 441-448",this.statusDate, 8))
                .append(DataUtil.rightPadding("Right padded-> oldSupplierBranchCode @Position 449-456",this.oldSupplierBranchCode, 8))
                .append(DataUtil.leftPadding("Left padded -> oldAccountNumber @Position 457-481",this.oldAccountNumber, 25))
                .append(DataUtil.leftPadding("Left padded -> oldSubAccountNumber @Position 482-485",this.oldSubAccountNumber, 4))
                .append(DataUtil.rightPadding("Right padded-> oldSupplierReferenceNumber @Position 486-495",this.oldSupplierReferenceNumber, 10))
                .append(DataUtil.leftPadding("Left padded -> homeTelephone @Position 496-511",this.homeTelephone, 16))
                .append(DataUtil.leftPadding("Left padded -> cellularTelephone @Position 512-527",this.cellularTelephone, 16))
                .append(DataUtil.leftPadding("Left padded -> workTelephone @Position 528-543",this.workTelephone, 16))
                .append(DataUtil.rightPadding("Right padded -> employerDetail @Position 544-603",this.employerDetail, 60))
                .append(DataUtil.leftPadding("Left padded -> income @Position @Position 604-612",this.income, 9))
                .append(DataUtil.rightPadding("Right padded -> incomeFrequency @Position 613-613",this.incomeFrequency, 1))
                .append(DataUtil.rightPadding("Right padded -> occupation @Position 614-633",this.occupation, 20))
                .append(DataUtil.rightPadding("Right padded -> thirdPartyName @Position 634-693",this.thirdPartyName, 60))
                .append(DataUtil.leftPadding("Left padded -> accountSoldToThirdParty @Position 694-695",this.accountSoldToThirdParty, 2))
                .append(DataUtil.leftPadding("Left padded -> noOfParticipantsInJointLoan @Position 696-698",this.noOfParticipantsInJointLoan, 3))
                .append(DataUtil.rightPadding("Right padded -> filler @Position 699-700",this.filler, 2));
        return s.toString();
    }

    @Override
    public String toString() {
        return "DataSegment{" +
                "data='" + data + '\'' +
                ", idNumber='" + idNumber + '\'' +
                ", otherIdNumberOrPassportNumber='" + otherIdNumberOrPassportNumber + '\'' +
                ", gender='" + gender + '\'' +
                ", dateOfBirth='" + dateOfBirth + '\'' +
                ", branchCode='" + branchCode + '\'' +
                ", accountNo='" + accountNo + '\'' +
                ", subAccountNo='" + subAccountNo + '\'' +
                ", surname='" + surname + '\'' +
                ", tittle='" + tittle + '\'' +
                ", foreNameOrInitial1='" + foreNameOrInitial1 + '\'' +
                ", foreNameOrInitial2='" + foreNameOrInitial2 + '\'' +
                ", foreNameOrInitial3='" + foreNameOrInitial3 + '\'' +
                ", residentialAddressLine1='" + residentialAddressLine1 + '\'' +
                ", residentialAddressLine2='" + residentialAddressLine2 + '\'' +
                ", residentialAddressLine3='" + residentialAddressLine3 + '\'' +
                ", residentialAddressLine4='" + residentialAddressLine4 + '\'' +
                ", postalCodeOfResidentialAddress='" + postalCodeOfResidentialAddress + '\'' +
                ", ownerOrTenant='" + ownerOrTenant + '\'' +
                ", postalAddressLine1='" + postalAddressLine1 + '\'' +
                ", postalAddressLine2='" + postalAddressLine2 + '\'' +
                ", postalAddressLine3='" + postalAddressLine3 + '\'' +
                ", postalAddressLine4='" + postalAddressLine4 + '\'' +
                ", postalCodeOfPostalAddress='" + postalCodeOfPostalAddress + '\'' +
                ", ownershipType='" + ownershipType + '\'' +
                ", loanReasonCode='" + loanReasonCode + '\'' +
                ", paymentType='" + paymentType + '\'' +
                ", typeOfAccount='" + typeOfAccount + '\'' +
                ", dateAccountOpened='" + dateAccountOpened + '\'' +
                ", deferredPaymentDate='" + deferredPaymentDate + '\'' +
                ", dateOnWhichLastPaymentWasReceived='" + dateOnWhichLastPaymentWasReceived + '\'' +
                ", openingBalanceOrCreditLimit='" + openingBalanceOrCreditLimit + '\'' +
                ", currentBalance='" + currentBalance + '\'' +
                ", currentBalanceIndicator='" + currentBalanceIndicator + '\'' +
                ", amountOverdue='" + amountOverdue + '\'' +
                ", instalmentAmount='" + instalmentAmount + '\'' +
                ", monthsInArrears='" + monthsInArrears + '\'' +
                ", statusCode='" + statusCode + '\'' +
                ", repaymentFrequency='" + repaymentFrequency + '\'' +
                ", terms='" + terms + '\'' +
                ", statusDate='" + statusDate + '\'' +
                ", oldSupplierBranchCode='" + oldSupplierBranchCode + '\'' +
                ", oldAccountNumber='" + oldAccountNumber + '\'' +
                ", oldSubAccountNumber='" + oldSubAccountNumber + '\'' +
                ", oldSupplierReferenceNumber='" + oldSupplierReferenceNumber + '\'' +
                ", homeTelephone='" + homeTelephone + '\'' +
                ", cellularTelephone='" + cellularTelephone + '\'' +
                ", workTelephone='" + workTelephone + '\'' +
                ", employerDetail='" + employerDetail + '\'' +
                ", income='" + income + '\'' +
                ", incomeFrequency='" + incomeFrequency + '\'' +
                ", occupation='" + occupation + '\'' +
                ", thirdPartyName='" + thirdPartyName + '\'' +
                ", accountSoldToThirdParty='" + accountSoldToThirdParty + '\'' +
                ", noOfParticipantsInJointLoan='" + noOfParticipantsInJointLoan + '\'' +
                ", filler='" + filler + '\'' +
                '}';
    }
}
