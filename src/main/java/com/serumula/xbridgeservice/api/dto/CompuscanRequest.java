package com.serumula.xbridgeservice.api.dto;


import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@AllArgsConstructor
@NoArgsConstructor
@Data
@SuperBuilder
@Builder
@Slf4j
@JsonPropertyOrder({
        "compuscanHeaderDto",
        "segmentDto"
})
public class CompuscanRequest {
    private CompuscanHeaderDto compuscanHeaderDto;
    private SegmentDto segmentDto;
}
