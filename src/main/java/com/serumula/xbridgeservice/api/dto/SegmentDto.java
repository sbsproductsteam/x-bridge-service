package com.serumula.xbridgeservice.api.dto;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.serumula.xbridgeservice.exception.InvalidInputException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@SuperBuilder
@Builder
@Slf4j
@JsonPropertyOrder({
        "headSegment",
        "dataSegments",
        "tailSegment"
})
public class SegmentDto {
    private HeadSegment headSegment;
    private List<DataSegment> dataSegments;
    private TailSegment tailSegment;

    public String toCompuscanString() throws InvalidInputException {
        StringBuilder stringBuilder = new StringBuilder();

            stringBuilder
                    .append( this.headSegment.toCompuScanString())
                    .append(" \n");

            for (final DataSegment dataSegment : this.dataSegments) {
                stringBuilder
                        .append( dataSegment.toCompuScanString())
                        .append(" \n");
            }
            stringBuilder
                    .append( this.tailSegment.toCompuScanString())
                    .append(" \n");

            return stringBuilder.toString();

    }
}
