package com.serumula.xbridgeservice.api.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@SuperBuilder
@Builder
public class BreauFileDto implements Serializable {
    private String headRow;
    private List<Data>  dataRows = new ArrayList<>();
    private String tailRow;

    @AllArgsConstructor
    @NoArgsConstructor
    @lombok.Data
    @SuperBuilder
    @Builder
    public static  class Data implements Serializable{
        String dataRow;

    }
}
