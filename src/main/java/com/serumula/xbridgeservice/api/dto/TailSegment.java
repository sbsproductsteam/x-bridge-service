package com.serumula.xbridgeservice.api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.serumula.xbridgeservice.api.util.DataUtil;
import com.serumula.xbridgeservice.exception.InvalidInputException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Data
@SuperBuilder
@Builder
@JsonPropertyOrder({
        "trailer",
        "numberOfRecordsSupplied",
        "filler"
})
public class TailSegment {
    /*1*/@JsonProperty(required = true) @NotBlank String trailer = "T"; //1-1 -> A1 ->M
    /*2*/String numberOfRecordsSupplied = ""; //2-10 -> N9 ->M
    /*1*/String filler = ""; //11-700

    public String toCompuScanString() throws InvalidInputException {
        StringBuilder s = new StringBuilder();
        s
                .append(DataUtil.rightPadding("Right padded -> trailer @position 1-1",this.trailer,1))
                .append(DataUtil.leftPadding("Left padded -> numberOfRecordsSupplied @position 2-12",this.numberOfRecordsSupplied, 11))
                .append(DataUtil.rightPadding("Right padded -> filler @position 13-700", this.filler, 688));
        return s.toString();
    }
}
