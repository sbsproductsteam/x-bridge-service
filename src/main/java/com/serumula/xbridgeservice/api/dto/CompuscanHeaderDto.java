package com.serumula.xbridgeservice.api.dto;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.serumula.xbridgeservice.exception.InvalidInputException;
import com.serumula.xbridgeservice.service.StringManipulationService;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@SuperBuilder
@Builder
@Slf4j
public class CompuscanHeaderDto {
    private SubmissionMode mode;
    private String destinationEmail;
    private String sftpPassword;
    private String sftpUsername;
    private String fileName;

    public static enum SubmissionMode{
        EMAIL("Email"),
        SFTP("sftp");

        SubmissionMode(String value) {

        }
    }
}
